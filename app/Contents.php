<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Contents extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'short_description',
        'content',
        'type',
        'image',
        'url',
        'link',
        'size',
        'year'
    ];

    public $rules = [
        'title' => 'required',
        'type'=>'required',
    ];

    public function images(){
        return $this->hasMany('App\ContentsImages');
    }

    public function categories(){
        return $this->belongsToMany('App\Categories','categories_has_contents');
    }
}
