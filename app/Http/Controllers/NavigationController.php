<?php

namespace App\Http\Controllers;

use App\Banners;
use App\Categories;
use App\Contents;
use App\Informations;
use Illuminate\Http\Request;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class NavigationController extends Controller
{

    public function __construct(Request $request)
    {
        $url = $request->url();
        $information = Informations::get()->first();

        View::share('url', $url);
        View::share('information', $information);
    }

    public function index(Request $request)
    {
        //pega os banners
        $banners = Banners::all();

        $featImg = Banners::get()->first();

        $about = Contents::where('type', '=', 'about')->get()->first();
        $clients = Contents::where('type', '=', 'carousel')->get();
        $logos = Contents::where('type', '=', 'cliente')->get();
        $page_contact = Contents::where('type', '=', 'contact')->get()->first();
        $blog = Contents::with('categories')->where('type', '=', 'blog')->get();
        $port = Contents::has('images')->where('type', '=', 'portifolio')->get();
        $informations = Informations::where('id', '=', '1')->get();
        $conteudo = Contents::where('type', '=', 'conteudo')->get()->first();

        $infos = Contents::where('type', '=', 'criamos')->get();



        $outra = Contents::where('type', '=', 'itens-quem-somos')->get();

        return view(
            'pages.index',
            [
                'banners' => $banners,
                'infos' => $infos,
                'about' => $about,
                'client' => $clients,
                'logos' => $logos,
                'page_contact' => $page_contact,
                'blog' => $blog,
                'port' => $port,
                'outra' => $outra,
                'informations' => $informations,
                'conteudo' => $conteudo,
                'featImg' => $featImg
            ]
        );
    }

    public function portfolio(Request $request) {

        $id = $request->route('id');

        $portfolio = Contents::where('id', '=', $id)->get()->first();
        $gallery = $portfolio->images()->get()->all();

        if(isset($portfolio->link) && !empty($portfolio->link)) {
            if(isset($portfolio->content) && !empty($portfolio->content))

            $portfolio->link = $this->getYouTubeVideoId($portfolio->link);
        }

        return view('pages.portfolio', ['portfolio' => $portfolio, 'gallery' => $gallery]);

    }

    public function sobre(Request $request)
    {
        $page = Contents::where('url', 'sobre-o-filme')->get()->first();
        return view(
            'pages.sobre',
            ['page' => $page]
        );
    }
    public function exiba(Request $request)
    {
        $page = Contents::where('url', 'exiba-o-filme')->get()->first();
        return view(
            'pages.exiba',
            ['page' => $page]
        );
    }
    public function calendario(Request $request)
    {
        $page = Contents::where('url', 'agenda-de-exibicoes')->get()->first();
        return view(
            'pages.agenda',
            ['page' => $page]
        );
    }

    public function festivais(Request $request)
    {
        $page = Contents::where('url', 'festivais')->get()->first();
        return view(
            'pages.festivais',
            ['page' => $page]
        );
    }

    public function noticias(Request $request)
    {
        $page = Contents::where('url', 'noticias')->get()->first();
        return view(
            'pages.noticias',
            ['page' => $page]
        );
    }

    public function mobilizacao(Request $request)
    {
        $page = Contents::where('url', 'mobilizacao-social')->get()->first();
        return view(
            'pages.mobilizacao',
            ['page' => $page]
        );
    }

    public function tese(Request $request)
    {
        $page = Contents::where('url', 'tese-doutorado')->get()->first();
        return view(
            'pages.tese',
            ['page' => $page]
        );
    }

    public function contato(Request $request)
    {
        $page = Contents::where('url', 'contato')->get()->first();
        return view(
            'pages.contato',
            ['page' => $page]
        );
    }

    public function blog(Request $request)
    {
        $url = $request->route('url');
        // $category = Categories::all();
        $page = Contents::with("categories")->where('type', '=', 'blog')->where('url', '=', $url)->get()->first();

        return view(
            'pages.blog',
            ['page' => $page]
        );
    }

    public function mail(Request $request)
    {

        $form = $request->all();

        Mail::to('edinaldo@agencialed.com.br')
            ->send(new SendMail($form, 'Formulário de Contato'));
        if (Mail::failures()) {
            exit;
        }
        return 'Email was sent';
    }
}
