<?php

namespace App\Http\Controllers;

use App\Mail\Contato;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function send(){
        Mail::to(['denis@agencialed.com.br'])
        ->send(new Contato());

        if (Mail::failures()) {
            echo '0';
            exit;
        }
        echo '1';
        }
}
