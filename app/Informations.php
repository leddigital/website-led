<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Informations extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'address',
        'number',
        'complement',
        'district',
        'zipcode',
        'city',
        'state',
        'whatsapp',
        'instagram',
        'facebook',
        'linkedin',
        'twitter',
        'youtube',
        'email',
        'phone1',
        'phone2',
        'meta_description'
    ];
}
