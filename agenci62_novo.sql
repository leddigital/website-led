-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 16/01/2020 às 12:10
-- Versão do servidor: 5.7.28-0ubuntu0.18.04.4
-- Versão do PHP: 7.2.26-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `agenci62_novo`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `info1` text,
  `info2` text,
  `info3` text,
  `link1` text,
  `link2` text,
  `link3` text,
  `path` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `image`, `title`, `info1`, `info2`, `info3`, `link1`, `link2`, `link3`, `path`, `created_at`, `updated_at`, `status`, `order`) VALUES
(1, 'banner_5dde8b8b27f3a.jpeg', 'logos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 14:43:23', '2019-11-27 14:43:23', NULL, NULL),
(2, 'banner_5dde8b9e29ffb.jpeg', 'logos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 14:43:42', '2019-11-27 14:43:42', NULL, NULL),
(3, 'banner_5dde8bb0810ff.jpeg', 'logos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 14:44:00', '2019-11-27 14:44:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `categories`
--

INSERT INTO `categories` (`id`, `title`, `url`, `type`) VALUES
(1, 'Tecnologia', 'tecnologia', 'blog'),
(2, 'TESTE', 'teste', 'blog');

-- --------------------------------------------------------

--
-- Estrutura para tabela `categories_has_contents`
--

CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `categories_has_contents`
--

INSERT INTO `categories_has_contents` (`categories_id`, `contents_id`) VALUES
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(2, 11);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `short_description` text,
  `content` longtext,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alta` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `contents`
--

INSERT INTO `contents` (`id`, `title`, `description`, `short_description`, `content`, `type`, `url`, `image`, `image_alta`, `created_at`, `updated_at`, `link`, `size`, `year`) VALUES
(4, 'logos', NULL, NULL, NULL, 'cliente', 'logos', '15753168035de56d4376508.jpeg', NULL, '2019-11-27 15:47:29', '2019-12-02 20:00:03', NULL, NULL, NULL),
(5, 'logos', NULL, NULL, NULL, 'cliente', 'logos-1', '15748698195dde9b3bacc1b.jpeg', NULL, '2019-11-27 15:50:19', '2019-11-27 15:50:19', NULL, NULL, NULL),
(6, 'Titulo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac dictum turpis, a tincidunt orci. Phasellus at ultrices nisl, ullamcorper accumsan erat. Integer ac tellus fringilla, aliquam nisi luctus, feugiat neque. Aenean nisi ex, suscipit non ante ac, tincidunt scelerisque ante. Praesent aliquam finibus augue sit amet tempor. Ut tincidunt quis tellus sed commodo. Nullam in magna sed dolor egestas fringilla.&nbsp;</p>', 'blog', 'titulo', '15748709115dde9f7f28ef0.jpeg', NULL, '2019-11-27 16:08:31', '2019-11-27 16:08:31', NULL, NULL, NULL),
(12, 'TESTE', NULL, NULL, NULL, 'cliente', 'teste-1', '15753168545de56d7620262.jpeg', NULL, '2019-12-02 20:00:54', '2019-12-02 20:00:54', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents_has_contents`
--

CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents_images`
--

CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text,
  `contents_id` int(11) NOT NULL,
  `path` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `contents_images`
--

INSERT INTO `contents_images` (`id`, `description`, `type`, `order`, `image`, `contents_id`, `path`) VALUES
(4, NULL, 'gallery', '0', '15791128685e1f59a454bbf.jpeg', 23, '/home/edinaldo/projetos/agencialed/public/content/23/'),
(5, NULL, 'gallery', '0', '15791128685e1f59a456ce8.jpeg', 23, '/home/edinaldo/projetos/agencialed/public/content/23/'),
(6, NULL, 'gallery', '0', '15791128685e1f59a458dec.jpeg', 23, '/home/edinaldo/projetos/agencialed/public/content/23/'),
(7, NULL, 'gallery', '0', '15791187725e1f70b4d4baa.jpeg', 27, '/home/edinaldo/projetos/agencialed/public/content/27/'),
(8, NULL, 'gallery', '0', '15791187725e1f70b4d6319.jpeg', 27, '/home/edinaldo/projetos/agencialed/public/content/27/'),
(9, NULL, 'gallery', '0', '15791187725e1f70b4d759a.jpeg', 27, '/home/edinaldo/projetos/agencialed/public/content/27/'),
(10, NULL, 'gallery', '0', '15791191855e1f7251ed5ba.jpeg', 30, '/home/edinaldo/projetos/agencialed/public/content/30/'),
(11, NULL, 'gallery', '0', '15791191855e1f7251eee82.jpeg', 30, '/home/edinaldo/projetos/agencialed/public/content/30/'),
(12, NULL, 'gallery', '0', '15791191855e1f7251f05e2.jpeg', 30, '/home/edinaldo/projetos/agencialed/public/content/30/'),
(13, NULL, 'gallery', '0', '15791839005e206f1c4891b.jpeg', 38, '/home/edinaldo/projetos/agencialed/public/content/38/'),
(14, NULL, 'gallery', '0', '15791839005e206f1c4a4cc.jpeg', 38, '/home/edinaldo/projetos/agencialed/public/content/38/'),
(15, NULL, 'gallery', '0', '15791839005e206f1c4ba35.jpeg', 38, '/home/edinaldo/projetos/agencialed/public/content/38/');

-- --------------------------------------------------------

--
-- Estrutura para tabela `informations`
--

CREATE TABLE `informations` (
  `id` int(11) NOT NULL,
  `address` text,
  `number` varchar(45) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(45) DEFAULT NULL,
  `instagram` text,
  `facebook` text,
  `linkedin` text,
  `twitter` text,
  `pinterest` text,
  `phone1` varchar(45) DEFAULT NULL,
  `email` text,
  `phone2` varchar(45) DEFAULT NULL,
  `youtube` text,
  `complement` text,
  `valor_1` text,
  `valor_2` text,
  `valor_3` text,
  `valor_4` text,
  `valor_5` text,
  `valor_texto_1` text,
  `valor_texto_2` text,
  `valor_texto_3` text,
  `valor_texto_4` text,
  `valor_texto_5` text,
  `meta_description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `informations`
--

INSERT INTO `informations` (`id`, `address`, `number`, `district`, `zipcode`, `city`, `state`, `whatsapp`, `instagram`, `facebook`, `linkedin`, `twitter`, `pinterest`, `phone1`, `email`, `phone2`, `youtube`, `complement`, `valor_1`, `valor_2`, `valor_3`, `valor_4`, `valor_5`, `valor_texto_1`, `valor_texto_2`, `valor_texto_3`, `valor_texto_4`, `valor_texto_5`, `meta_description`) VALUES
(1, 'Rua Santo André', '185', 'Jardim Europa', '15014-490', 'São José do Rio Preto', 'SP', '17 98118 8655', 'https://www.instagram.com/ledcomunicacao/', 'https://pt-br.facebook.com/ledcomunicacao/', 'https://www.linkedin.com/company/ag-ncia-led-comunica-o/', NULL, NULL, '17 3301 0401', 'contato@agencialed.com.br', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Somos especialistas em concretizar ideias que atendam aos objetivos, prazos e verba dos clientes. Sabemos que o discursos bonitos podem inspirar, mas o trabalho é responsável por transformar a imagem e os números de uma empresa. A propaganda faz a diferença na vida do consumidor e no negócio do cliente, o trabalho é dobrado, a satisfação também.');

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'leddigital', 'digital@agencialed.com.br', NULL, '$2y$10$6NsTVvIenPGcOpcmLpjxIeS5p5Q4oTK59ThKMj9kuCnRgIwisgUgq', 'fRxlJZDRSNlkoUzwym7xsJ9XUQvn5Qe9E5x951FoO5Wlqxy61qpoTYDWuXp8', '2019-09-26 20:22:30', '2019-09-26 20:22:30'),
(4, 'denis', 'denis@agencialed.com.br', NULL, '$2y$10$DVwJO1U6VyPghXCLGG7S0uv4BFS4B.3.aZ2Na.ak0Yo8QMmHg.h62', NULL, '2019-11-26 20:58:45', '2019-11-26 20:58:45'),
(5, 'Agencia LED', 'edinaldo@agencialed.com.br', NULL, '$2y$10$gdqIXYbRgvbCcAc6AnFytOAxo8yXX0qqEzS8sb9fkOe/LB3A5bg6y', NULL, '2019-12-02 20:34:12', '2019-12-02 20:34:12');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categories_has_contents`
--
ALTER TABLE `categories_has_contents`
  ADD PRIMARY KEY (`categories_id`,`contents_id`),
  ADD KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  ADD KEY `fk_categories_has_contents_categories1_idx` (`categories_id`);

--
-- Índices de tabela `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_UNIQUE` (`url`);

--
-- Índices de tabela `contents_has_contents`
--
ALTER TABLE `contents_has_contents`
  ADD PRIMARY KEY (`contents_id`,`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents1_idx` (`contents_id`);

--
-- Índices de tabela `contents_images`
--
ALTER TABLE `contents_images`
  ADD PRIMARY KEY (`id`,`contents_id`),
  ADD KEY `fk_contents_images_contents_idx` (`contents_id`);

--
-- Índices de tabela `informations`
--
ALTER TABLE `informations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de tabela `contents_images`
--
ALTER TABLE `contents_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de tabela `informations`
--
ALTER TABLE `informations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
