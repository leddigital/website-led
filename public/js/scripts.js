$(document).ready(function() {

    if($(window).width() <= 768) {
        $('#inner-slide').css('display', 'none');
        $('#home').css('background-image', 'url(http://local.agencialed.com.br/banners/banner_5dde8b8b27f3a.jpeg)');
    } else {
       $(window).resize(function(){
            if($(window).width() <= 768) {
                $('#inner-slide').css('display', 'none');
                $('#home').css('background-image', 'url(http://local.agencialed.com.br/banners/banner_5dde8b8b27f3a.jpeg)');
            } else {
                $('#inner-slide').css('display', 'block');
                $('#home').css('background-image', 'none');
            }
       })
    }

});


$('#contact-form').submit(function(e){
    e.preventDefault();

    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        beforeSend: function(data) {
            $('#status').text('Enviando...')
            $("#contact-form :input").prop("disabled", true);
        },
        success: function(data) {
            $('#status').text('Mensagem enviada com sucesso!')
        },
        error: function(data) {
            $('#status').text('Falha ao enviar mensagem. Tente novamente mais tarde.')
        }
    })

});
