/* Recupera o valor do tamanhoda imagem requerido pelo usuário */

var size = [];

$('#image').prop('disabled', true);

$(document).ready(function(){
    $('#imageSize').on('change', function(){
        size['width'] = $(this).find(':selected').data('width');
        size['height'] = $(this).find(':selected').data('height');

        if ($(this).val() == "none"){
            $('#image').prop('disabled', true);
            $('#base64').html('');
            $('#output').attr('src', '')
        } else {
            $('#image').prop('disabled', false);
        }
    });
});



function getSizeWidth() {
    return size['width'];
}

function getSizeHeight() {
    return size['height'];
}


/* Verifica qual tab está ativa no momento */



