-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 27-Nov-2019 às 20:27
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `agencialed`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `info1` text DEFAULT NULL,
  `info2` text DEFAULT NULL,
  `info3` text DEFAULT NULL,
  `link1` text DEFAULT NULL,
  `link2` text DEFAULT NULL,
  `link3` text DEFAULT NULL,
  `path` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `image`, `title`, `info1`, `info2`, `info3`, `link1`, `link2`, `link3`, `path`, `created_at`, `updated_at`, `status`, `order`) VALUES
(1, 'banner_5dde8b8b27f3a.jpeg', 'logos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 14:43:23', '2019-11-27 14:43:23', NULL, NULL),
(2, 'banner_5dde8b9e29ffb.jpeg', 'logos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 14:43:42', '2019-11-27 14:43:42', NULL, NULL),
(3, 'banner_5dde8bb0810ff.jpeg', 'logos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 14:44:00', '2019-11-27 14:44:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `title`, `url`, `type`) VALUES
(1, 'Tecnologia', 'tecnologia', 'blog');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories_has_contents`
--

CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categories_has_contents`
--

INSERT INTO `categories_has_contents` (`categories_id`, `contents_id`) VALUES
(1, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `short_description` text DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_alta` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contents`
--

INSERT INTO `contents` (`id`, `title`, `description`, `short_description`, `content`, `type`, `url`, `image`, `image_alta`, `created_at`, `updated_at`, `link`) VALUES
(2, 'Titulo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', NULL, NULL, 'criamos', 'titulo-1', '15747984755ddd848bdbea4.jpeg', NULL, '2019-11-26 20:01:15', '2019-11-26 20:01:15', 'https://www.youtube.com/watch?v=HvLNO1LZdZI'),
(4, 'logos', NULL, NULL, NULL, 'cliente', 'logos', '15748696495dde9a911b0f2.jpeg', NULL, '2019-11-27 15:47:29', '2019-11-27 15:47:29', NULL),
(5, 'logos', NULL, NULL, NULL, 'cliente', 'logos-1', '15748698195dde9b3bacc1b.jpeg', NULL, '2019-11-27 15:50:19', '2019-11-27 15:50:19', NULL),
(6, 'Titulo', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac dictum turpis, a tincidunt orci. Phasellus at ultrices nisl, ullamcorper accumsan erat. Integer ac tellus fringilla, aliquam nisi luctus, feugiat neque. Aenean nisi ex, suscipit non ante ac, tincidunt scelerisque ante. Praesent aliquam finibus augue sit amet tempor. Ut tincidunt quis tellus sed commodo. Nullam in magna sed dolor egestas fringilla.&nbsp;</p>', 'blog', 'titulo', '15748709115dde9f7f28ef0.jpeg', NULL, '2019-11-27 16:08:31', '2019-11-27 16:08:31', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contents_has_contents`
--

CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contents_images`
--

CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `contents_id` int(11) NOT NULL,
  `path` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `informations`
--

CREATE TABLE `informations` (
  `id` int(11) NOT NULL,
  `address` text DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(45) DEFAULT NULL,
  `instagram` text DEFAULT NULL,
  `facebook` text DEFAULT NULL,
  `linkedin` text DEFAULT NULL,
  `twitter` text DEFAULT NULL,
  `pinterest` text DEFAULT NULL,
  `phone1` varchar(45) DEFAULT NULL,
  `email` text DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `youtube` text DEFAULT NULL,
  `complement` text DEFAULT NULL,
  `valor_1` text DEFAULT NULL,
  `valor_2` text DEFAULT NULL,
  `valor_3` text DEFAULT NULL,
  `valor_4` text DEFAULT NULL,
  `valor_5` text DEFAULT NULL,
  `valor_texto_1` text DEFAULT NULL,
  `valor_texto_2` text DEFAULT NULL,
  `valor_texto_3` text DEFAULT NULL,
  `valor_texto_4` text DEFAULT NULL,
  `valor_texto_5` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `informations`
--

INSERT INTO `informations` (`id`, `address`, `number`, `district`, `zipcode`, `city`, `state`, `whatsapp`, `instagram`, `facebook`, `linkedin`, `twitter`, `pinterest`, `phone1`, `email`, `phone2`, `youtube`, `complement`, `valor_1`, `valor_2`, `valor_3`, `valor_4`, `valor_5`, `valor_texto_1`, `valor_texto_2`, `valor_texto_3`, `valor_texto_4`, `valor_texto_5`) VALUES
(1, 'Rua Santo André', '185', 'Jardim Europa', '15014-490', 'São José do Rio Preto', 'SP', '17 98118 8655', 'https://www.instagram.com/ledcomunicacao/', 'https://pt-br.facebook.com/ledcomunicacao/', 'https://www.linkedin.com/company/ag-ncia-led-comunica-o/', NULL, NULL, '17 3301 0401', 'contato@agencialed.com.br', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'leddigital', 'digital@agencialed.com.br', NULL, '$2y$10$6NsTVvIenPGcOpcmLpjxIeS5p5Q4oTK59ThKMj9kuCnRgIwisgUgq', 'fRxlJZDRSNlkoUzwym7xsJ9XUQvn5Qe9E5x951FoO5Wlqxy61qpoTYDWuXp8', '2019-09-26 20:22:30', '2019-09-26 20:22:30'),
(4, 'denis', 'denis@agencialed.com.br', NULL, '$2y$10$DVwJO1U6VyPghXCLGG7S0uv4BFS4B.3.aZ2Na.ak0Yo8QMmHg.h62', NULL, '2019-11-26 20:58:45', '2019-11-26 20:58:45');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `categories_has_contents`
--
ALTER TABLE `categories_has_contents`
  ADD PRIMARY KEY (`categories_id`,`contents_id`),
  ADD KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  ADD KEY `fk_categories_has_contents_categories1_idx` (`categories_id`);

--
-- Índices para tabela `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_UNIQUE` (`url`);

--
-- Índices para tabela `contents_has_contents`
--
ALTER TABLE `contents_has_contents`
  ADD PRIMARY KEY (`contents_id`,`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents1_idx` (`contents_id`);

--
-- Índices para tabela `contents_images`
--
ALTER TABLE `contents_images`
  ADD PRIMARY KEY (`id`,`contents_id`),
  ADD KEY `fk_contents_images_contents_idx` (`contents_id`);

--
-- Índices para tabela `informations`
--
ALTER TABLE `informations`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `contents_images`
--
ALTER TABLE `contents_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `informations`
--
ALTER TABLE `informations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `categories_has_contents`
--
ALTER TABLE `categories_has_contents`
  ADD CONSTRAINT `fk_categories_has_contents_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_categories_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `contents_has_contents`
--
ALTER TABLE `contents_has_contents`
  ADD CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `contents_images`
--
ALTER TABLE `contents_images`
  ADD CONSTRAINT `fk_contents_images_contents` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
