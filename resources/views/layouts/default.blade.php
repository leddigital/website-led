<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    @hasSection('meta_descritpion')
        <meta name="description" content="@yield('meta_descritpion')">
    @else
        <meta name="description" content="{{ isset($information->meta_description) && !empty($information->meta_description) ? $information->meta_description : "" }}">
    @endif

    <meta name="author" content="Agência LED">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="@yield('fb_meta_title')">
    <meta property="og:description" content="@yield('fb_meta_description')">
    <meta property="og:image" content="@yield('fb_meta_image')">
    <meta property="og:image:alt" content="@yield('fb_meta_image_alt')">

    <meta property="og:image:width" content="@yield('fb_meta_image_width')" />
    <meta property="og:image:height" content="@yield('fb_meta_image_height')" />
    <meta property="og:url" content="@yield('fb_meta_url')">

    <meta name="twitter:title" content="@yield('tw_meta_title')">
    <meta name="twitter:description" content="@yield('tw_meta_description')">
    <meta name="twitter:image" content="@yield('tw_meta_image')">
    <meta name="twitter:card" content="@yield('tw_meta_card')">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />
    <title>Agência Led</title>

    <link rel="stylesheet" href="{{ asset('css/bootstrap-reboot.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-grid.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/nivoslider/nivo-slider.css') }}">

    <link rel="stylesheet" href="{{ asset('plugins/lity/lity.min.css') }}">
    {{-- <link href="https://fonts.googleapis.com/css?family=Lora:400,700&display=swap" rel="stylesheet"> --}}
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Poppins:100,200,300,400,400i,500,600,700%7CMontserrat:700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}" />

</head>

<body class="page-template-onepage">

    @include('blocks.header')
    @yield('content')
    @include('blocks.footer')

    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/slick/slick.js') }}"></script>
    <script src="{{ asset('plugins/lity/lity.min.js') }}"></script>
    <script src="{{ asset('plugins/nivoslider/jquery.nivo.slider.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="{{ asset('js/jquery.js')}}"></script>
    <script src="{{ asset('js/jquery.sticky.js')}}"></script>
    <script src='{{ asset('js/imagesloaded.pkgd.js')}}'></script>
    <script src='{{ asset('js/jquery.fitvids.js')}}'></script>
    <script src='{{ asset('js/jquery.smartmenus.min.js')}}'></script>
    <script src='{{ asset('js/isotope.pkgd.js')}}'></script>
    <script src='{{ asset('js/jquery.easing.1.3.js')}}'></script>
    <script src='{{ asset('js/jquery.prettyPhoto.js')}}'></script>
    <script src='{{ asset('js/owl.carousel.min.js')}}'></script>
    <script src='{{ asset('js/jquery.sticky-kit.min.js')}}'></script>
    <script src='{{ asset('js/main.js')}}'></script>
    <script src="{{ asset('js/scripts.js')}}"></script>

</body>

</html>
