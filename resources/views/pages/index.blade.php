@extends('layouts.default')

{{-- META TAGS --}}

@section('fb_meta_title', 'Agencia LED - Falar sobre ideias é falar sobre negócios.')
@section('fb_meta_description', isset($information->meta_description) && !empty($information->meta_description) ? $information->meta_description : "")
@section('fb_meta_image', asset('banners/' . $featImg->image))
@section('fb_meta_image_alt', asset('/img/logo_led.png'))
@section('fb_meta_url', route('nav.index'))
@section('fb_meta_image_width', '1920')
@section('fb_meta_image_height', '836')

@section('tw_meta_title', 'Agencia LED - Falar sobre ideias é falar sobre negócios.')
@section('tw_meta_description', isset($information->meta_description) && !empty($information->meta_description) ? $information->meta_description : "")
@section('tw_meta_image', asset('banners/' . $featImg->image))
@section('tw_meta_card', route('nav.index'))

{{-- META TAGS --}}

@section('content')
<div id="content" class="site-content center-relative">
    <!-- Home -->
    <div id="home" class="section full-screen full-width">
        <div id="inner-slide" class="section-wrapper block center-relative">
            <div class="content-wrapper">
                <script>
                    var slider1_speed = "2000";
                    var slider1_auto = "true";
                    var slider1_hover = "true";

                </script>
                <div class="image-slider-wrapper full-screen relative">
                    <div id="slider1" class="owl-carousel owl-theme image-slider slider">

                        @foreach ($banners as $item)
                        <div class="owl-item ">
                            <a href="#" class="scroll">
                                <img src="{{ asset('banners/'.$item->image)}}" alt="">
                            </a>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Sobre -->
    <div id="about" class="section page-split">
        <div class="section-wrapper block content-1170 center-relative">
            <div class="bg-holder float-right">
                <div class="split-color"></div>
            </div>
            <div class="sticky-spacer">
                <div class="section-title-holder float-right">
                    <div class="section-top-image">

                    </div>
                    <h2 class="entry-title">
                        SOMOS
                    </h2>
                    <p class="page-desc">
                        Assim enxergamos
                    </p>
                </div>
            </div>
            <div class="section-content-holder float-left">
                <div class="content-wrapper">
                    <div class="info-text">Falar sobre ideias é falar sobre negócios.
                        <br><br />
                        Assim enxergamos os números através da publicidade.</div>
                    <p>&nbsp;</p>

                    <div class="animate">
                        <div class="service-txt">
                            <h4>Pensamos fora da agência.</h4>
                            Somos especialistas em concretizar ideias que atendam aos objetivos, prazos e verba dos
                            clientes.
                            <br><br />
                            Sabemos que o discursos bonitos podem inspirar, mas o trabalho é responsável por transformar
                            a imagem e os números de uma empresa.
                            <br><br />
                            A propaganda faz a diferença na vida do consumidor e no negócio do cliente, o trabalho é
                            dobrado, a satisfação também.
                            <br><br />
                            As palavras difíceis ficam pra fora da sala de reunião, nosso papo é direto. Até por isso,
                            temos opinião sobre o futuro de cada cliente, pois permanecemos imersos em sua realidade,
                            sem ter a visão do mundo apenas atrás de uma mesa.
                            <br><br />
                            O mundo muda e a gente vai junto, tendo novas ideias e gerando novas oportunidades.

                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <!--SERVICES-->
    <div id="services" class="section page-split">
        <div class="section-wrapper block content-1170 center-relative">
            <div class="bg-holder float-left">
                <div class="split-color"></div>
            </div>
            <div class="sticky-spacer">
                <div class="section-title-holder float-left">
                    <div class="section-top-image">

                    </div>
                    <h2 class="entry-title">
                        FAZE <br>
                        MOS
                    </h2>
                    <p class="page-desc">
                        Ideias são como produtos. Se não funcionam, ninguém compra
                    </p>
                </div>
            </div>

            <div class="section-content-holder float-right">
                <div class="content-wrapper">
                    <div class="info-text center-text">Quem compra uma ideia tem a mesma expectativa. </div>
                    <p>&nbsp;</p>

                    <div class="one_half animate">
                        <div class="service-holder">
                            <div class="service-img">

                            </div>
                            <div class="service-txt">
                                <h4>PROPAGANDA</h4>
                                O foco da agência. É a ferramenta mais eficaz de persuasão de um público.
                                Capaz de induzir a compra de produtos e formação de opinião sobre marcas. Para isso,
                                utilizando meios de comunicação, posicionamento estratégico e criação.
                            </div>
                        </div>
                    </div>

                    <div class="one_half last animate">
                        <div class="service-holder">
                            <div class="service-img">

                            </div>
                            <div class="service-txt">
                                <h4>DESIGN</h4>
                                Onde damos traços e cores às ideias,
                                de forma estratégica e voltada para a melhor experiência do consumidor em cada ponto de
                                contato.
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="one_half animate">
                        <div class="service-holder">
                            <div class="service-img">

                            </div>
                            <div class="service-txt">
                                <h4>DIGITAL</h4>
                                Levamos o público a uma jornada de compra pela internet, baseada em conteúdo,
                                relacionamento, mídia e gestão de contatos para geração de vendas.
                            </div>
                        </div>
                    </div>

                    <div class="one_half last animate">
                        <div class="service-holder">
                            <div class="service-img">

                            </div>
                            <div class="service-txt">
                                <h4>PROMO & ATIVAÇÃO</h4>
                                Proporcionamos experiência de marca fora de canais de mídia, o que conduz o público a
                                uma resposta de compra e gera interação com o seu produto.
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="one_half animate">
                        <div class="service-holder">
                            <div class="service-img">

                            </div>
                            <div class="service-txt">
                                <h4>MARKETING</h4>
                                Ajudamos na organização e estruturação as áreas de marketing dos nossos clientes. Com
                                aprofundamento em dados de mercado, cenários, perfil de público, e proposta de geração
                                de valor.
                            </div>
                        </div>
                    </div>

                    <div class="one_half last animate">
                        <div class="service-holder">
                            <div class="service-img">

                            </div>
                            <div class="service-txt">
                                <h4>BRANDING</h4>
                                Muito além da criação de identidades visuais, trabalhamos com a criação de cultura e
                                atmosfera de marcas que inspiram.
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <!-- Clients -->
    <div id="clients" class="section page-split">
        <div class="section-wrapper block content-1170 center-relative">
            <div class="content-wrapper">
                <div class="bg-holder float-right">
                    <div class="split-color" style="background: #121011;"></div>
                </div>
                <div class="sticky-spacer">
                    <div class="section-title-holder float-right portfolio-title-fix-class">
                        <div class="section-top-image">

                        </div>
                        <h2 class="entry-title">
                            ATEN<br>DE<br>MOS
                        </h2>
                        <p class="page-desc">
                            Marcas pelas quais nos apaixonamos
                        </p>
                    </div>
                </div>
                <div class="section-content-holder float-left">
                    <div class="float-left">
                        <!-- Clientes logos colunas um -->
                        @foreach ($logos as $item)
                        <div class="one_half {{$loop->index % 2 == 0 ? "":"last" }} top-80">
                            <img src="{{ asset('content/'.$item->id.'/'.$item->image)}}" alt=""
                                class="bottom-25 aligncenter" id="logo-h">
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <!-- Portfolio -->
    <div id="portfolio" class="section page-split">
        <div class="section-wrapper block content-1170 center-relative">
            <div class="bg-holder float-left">
                <div class="split-color"></div>
            </div>
            <div class="sticky-spacer">
                <div class="section-title-holder float-left portfolio-title-fix-class">
                    <div class="section-top-image">

                    </div>
                    <h2 class="entry-title">
                        CRIA<br>MOS
                    </h2>
                    <p class="page-desc">
                        Traços, cores e texto, com muita transpiração e inspiração
                    </p>
                </div>
            </div>

            <div class="section-content-holder float-right">
                <div class="content-wrapper">
                    <div id="portfolio-wrapper">
                        <div class="portfolio-load-content-holder"></div>
                        <div class="grid" id="portfolio-grid">
                            <div class="grid-sizer"></div>

                            @foreach ($infos as $item)
                            <div class="grid-item element-item {{ $item->size }}">
                                <a class="item-link {{ isset($item->content) && !empty($item->content) ? "ajax-portfolio" : "" }}"
                                    href="{{ isset($item->content) && !empty($item->content) ? route('nav.portfolio', ['id' => $item->id]) : (isset($item->link) && !empty($item->link) ? $item->link : asset('content/'.$item->id.'/'.$item->image)) }}"
                                    data-rel="{{ !isset($item->content) && empty($item->content) ? "prettyPhoto[portfolio]" : "" }}"
                                    data-id="{{ $item->id }}">

                                    <img src="{{ asset('content/'.$item->id.'/'.$item->image)  }}" alt="">
                                    <div class="portfolio-text-holder">
                                        <p class="portfolio-title">{{ $item->title }}</p>
                                        <p class="portfolio-desc">{{ $item->description }}</p>
                                    </div>
                                </a>
                            </div>
                            @endforeach

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <!-- Inspire - Blog -->
    <div id="news" class="section page-split">
        <div class="section-wrapper block content-1170 center-relative">
            <div class="bg-holder float-right">
                <div class="split-color"></div>
            </div>
            <div class="sticky-spacer">
                <div class="section-title-holder float-right">
                    <h2 class="entry-title" id="blog">
                        BLOG
                    </h2>
                    <p class="page-desc" id="blog">
                        Porque um bom conteúdo nunca é demais
                    </p>
                </div>
            </div>

            <div class="section-content-holder float-left">
                <div class="content-wrapper">
                    <div class="blog-holder-scode latest-posts-scode block center-relative">
                        @foreach ($blog as $item)

                        <article class="relative blog-item-holder-scode">
                            <div class="entry-date published">{{ date('M d, Y',strtotime($item->created_at)) }}
                            </div>
                            <div class="cat-links">
                                <ul>
                                    @foreach ($item->categories as $cat)
                                    <li>
                                        <a href="{{ route('nav.post',['url'=>$item->url])}}">
                                            {{$cat->title}}
                                        </a>
                                    </li>

                                    @endforeach
                                </ul>
                            </div>
                            <h4 class="entry-title">
                                <a href="{{ route('nav.post',['url'=>$item->url])}}">
                                    {{$item->title}}
                                    <span class="arrow"></span>
                                </a>
                            </h4>
                            <div class="excerpt">
                                {{$item->description}}
                            </div>
                        </article>

                        @endforeach
                        {{-- <article class="relative blog-item-holder-scode">
                                <div class="entry-date published">January 26, 2018</div>
                                <div class="cat-links">
                                    <ul>
                                        <li>
                                            <a href="{{ route('blog')}}">Uncategorized</a>
                        </li>
                        </ul>
                    </div>
                    <h4 class="entry-title">
                        <a href="{{ route('blog')}}">
                            Muse about a very small stage in a vast arena
                            <span class="arrow"></span>
                        </a>
                    </h4>
                    <div class="excerpt">
                        Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture, hydrogen
                        atoms
                        cosmic fugue across ...
                    </div>
                    </article>

                    <article class="relative blog-item-holder-scode">
                        <div class="entry-date published">January 25, 2018</div>
                        <div class="cat-links">
                            <ul>
                                <li>
                                    <a href="{{ route('blog')}}">Uncategorized</a>
                                </li>
                            </ul>
                        </div>
                        <h4 class="entry-title">
                            <a href="{{ route('blog')}}">
                                Are creatures of the vast cosmos of global death
                                <span class="arrow"></span>
                            </a>
                        </h4>
                        <div class="excerpt">
                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture,
                            hydrogen
                            atoms cosmic fugue across ...
                        </div>
                    </article>

                    <article class="relative blog-item-holder-scode">
                        <div class="entry-date published">January 24, 2018</div>
                        <div class="cat-links">
                            <ul>
                                <li>
                                    <a href="{{ route('blog')}}">Uncategorized</a>
                                </li>
                            </ul>
                        </div>
                        <h4 class="entry-title">
                            <a href="{{ route('blog')}}">
                                Preserve and cherish that pale blue dot star stuff
                                <span class="arrow"></span>
                            </a>
                        </h4>
                        <div class="excerpt">
                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture,
                            hydrogen
                            atoms cosmic fugue across ...
                        </div>
                    </article>

                    <article class="relative blog-item-holder-scode">
                        <div class="entry-date published">January 23, 2018</div>
                        <div class="cat-links">
                            <ul>
                                <li>
                                    <a href="{{ route('blog')}}">Uncategorized</a>
                                </li>
                            </ul>
                        </div>
                        <h4 class="entry-title">
                            <a href="{{ route('blog')}}">
                                Kindling the energy hidden in matter vanquish now
                                <span class="arrow"></span>
                            </a>
                        </h4>
                        <div class="excerpt">
                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture,
                            hydrogen
                            atoms cosmic fugue across ...
                        </div>
                    </article>

                    <article class="relative blog-item-holder-scode">
                        <div class="entry-date published">January 22, 2018</div>
                        <div class="cat-links">
                            <ul>
                                <li>
                                    <a href="{{ route('blog')}}">Uncategorized</a>
                                </li>
                            </ul>
                        </div>
                        <h4 class="entry-title">
                            <a href="{{ route('blog')}}">
                                Galaxyrise great turbulent clouds colonies
                                <span class="arrow"></span>
                            </a>
                        </h4>
                        <div class="excerpt">
                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture,
                            hydrogen
                            atoms cosmic fugue across ...
                        </div>
                    </article> --}}
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="team" style="background: #8900b0" class="section page-split">
    <div class="section-wrapper block content-1170 center-relative">
        <div class="content-wrapper">
            <div class="bg-holder float-left">
                <div class="split-color" style="background: #8900b0;"></div>
            </div>
            <div class="sticky-spacer">
                <div class="section-title-holder float-left portfolio-title-fix-class">
                    <div class="section-top-image">
                        <img src="{{ asset('img/logo_leag.png') }}" alt="">
                    </div>
                    <h2 class="entry-title">
                        LEAG
                    </h2>
                    <p class="page-desc" style="font-size: 18px;">
                        Entre as maiores redes de agências independentes do mundo, a Led é exclusividade LEAG em Rio
                        Preto
                    </p>
                </div>
            </div>
            <div class="section-content-holder fd-right float-right" style="background-image: url({{asset('img/logo-da-leag.png')}});
                background-repeat: no-repeat;
                background-position:center;">
                <div class="float-right">
                    <div class="animate">
                        <div class="service-txt">
                            <div class="info-text">A LEAG – LOCAL EXPERT AGENCY GROUP – é a primeira rede global de
                                agências locais
                                escolhidas criteriosamente cidade por cidade, mercado por mercado. E a Agência Led é
                                exclusividade LEAG em São José do Rio Preto/SP.</div>
                            Formada por agências independentes, e seus fornecedores recomendados, a LEAG reúne o melhor
                            dos dois mundos:<br><br />EXPERTS locais que conhecem como ninguém seu mercado específico e
                            especialistas das mais diferentes disciplinas da comunicação <br><br /> São serviços de alta
                            qualidade
                            aliados a uma precisão de resultados que nenhuma outra rede de agências presas às grandes
                            capitais poderia alcançar <br><br />
                            O talento e a qualidade podem estar espalhados pelo mundo todo: através da LEAG estarão
                            unidos. Presença em mais de 15 países <br><br />

                            A Agência Led é exclusividade LEAG em São José do Rio Preto.
                            <br><br />
                            <div class="one_half last">Fale conosco e use todo esse potencial para a sua marca. 
                                <br><br /><i class="fa fa-phone"></i> 17 3301 0401

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<!-- Contact -->
<div id="contact" class="section page-split">
    <div class="section-wrapper block content-1170 center-relative">
        <div class="bg-holder float-right">
            <div class="split-color"></div>
        </div>
        <div class="sticky-spacer">
            <div class="section-title-holder float-right">
                <h2 class="entry-title">
                    CON<br>TATO
                </h2>
                <p class="page-desc">
                    Queremos que o seu problema seja da nossa conta
                </p>
            </div>
        </div>

        <div class="section-content-holder float-left">
            <div class="content-wrapper">
                <div class="info-text">
                    Ligue, mande um zap ou agende um café.
                </div>
                <p>&nbsp;</p>

                <div class="contact-form">
                    <form id="contact-form" action="{{route('email')}}" method="POST">
                        @csrf
                        <p><input id="name" type="text" name="name" placeholder="Nome" required></p>
                        <p><input id="contact-email" type="email" name="email" placeholder="Email" required></p>
                        <p><input id="subject" type="text" name="subject" placeholder="Tema" required></p>
                        <p><textarea id="message" name="messageBody" placeholder="Mensagem" required></textarea></p>
                        <p class="contact-submit-holder"><input type="submit" value="ENVIAR" required></p>
                        <div id="status" class="info-text text-white">
                            <p id="status"></p>
                        </div>
                    </form>
                </div>

                <p>&nbsp;</p>

                <div class="one_half">

                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
@endsection
