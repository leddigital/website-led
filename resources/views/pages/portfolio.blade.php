<div id="content" class="site-content center-relative">
    <div class="portfolio-item-wrapper center-relative">
        <div class="portfolio-content">

            <div class="info-text">{{ $portfolio->description }}</div>
            <script>
                var slider2_speed = "2000";
                var slider2_auto = "true";
                var slider2_hover = "true";
                var slider2_autoplay = "true";
            </script>

            @if (isset($portfolio->link) && !empty($portfolio->link))
                <p>
                    <iframe width="640" height="360" src="https://www.youtube.com/embed/{{ $portfolio->link }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    {{-- <iframe src="{{ $portfolio->link }}" width="640" height="360" allowfullscreen></iframe> --}}
                </p>
                <p>&nbsp;</p>
            @else
                <div id="slider2" class="image-slider-wrapper relative">
                    <div id="slider" class="owl-carousel owl-theme image-slider slider">

                        @foreach ($gallery as $singleImage)
                            <div class="owl-item">
                                <img src="{{ asset('content/' . $portfolio->id . "/gallery/" . $singleImage->image) }}" alt="">
                            </div>
                        @endforeach

                    </div>
                </div>
            @endif

            <div class="two_third">
                {!! $portfolio->content !!}
            </div>

            <div class="one_third last ">
                <p>
                    <strong>Nome do Projeto</strong><br>
                    {{ $portfolio->title }}
                </p>
                <p>
                    <strong>Ano</strong><br>
                    {{ $portfolio->year }}
                </p>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>
