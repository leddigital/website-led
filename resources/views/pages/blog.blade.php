@extends('layouts.default')

{{-- META TAGS --}}

@section('meta_descritpion', $page->title . ' - Agência LED')

@section('fb_meta_title', $page->title . ' - Agência LED')
@section('fb_meta_description', str_limit(strip_tags($page->content), $limit = 150, $end = "..."))
@section('fb_meta_image', asset('content/'.$page->id.'/'.$page->image))
@section('fb_meta_image_alt', asset('/img/logo_led.png'))
@section('fb_meta_url', route('nav.post', ['url' => $page->url]))
@section('fb_meta_image_width', '690')
@section('fb_meta_image_height', '345')

@section('tw_meta_title', $page->title . ' - Agência LED')
@section('tw_meta_description', str_limit(strip_tags($page->content), $limit = 150, $end = "..."))
@section('tw_meta_image', asset('content/'.$page->id.'/'.$page->image))
@section('tw_meta_card', route('nav.post', ['url' => $page->url]))


{{-- META TAGS --}}

@section('content')
<div class="site-wrapper">
        <div id="content" class="site-content center-relative">
            <div class="portfolio-item-wrapper center-relative">
                <div class="portfolio-content">
                    <div class="info-text">{{$page->title}}</div>
                    <script>
                        var slider2_speed = "2000";
                        var slider2_auto = "true";
                        var slider2_hover = "true";
                    </script>
                    <div class="img-post img-fluid" id="img-post">
                        <img src="{{ asset('content/'.$page->id.'/'.$page->image) }}">
                    </div>
                    {{-- <div id="slider2" class="image-slider-wrapper relative">
                        <div id="slider" class="owl-carousel owl-theme image-slider slider">
                            <div class="owl-item active">
                                <img src="{{ asset('img/img_08s.jpg')}}" alt="one">
                            </div>
                            <div class="owl-item">
                                <img src="{{ asset('img/img_12s.jpg')}}" alt="two">
                            </div>
                        </div>
                    </div> --}}

                    <div class="two_third">
                       {{$page->description}}
                    </div>

                    <div class="one_third last ">
                        <p>
                            <strong>Categoria</strong><br>
                            @foreach ($page->categories as $item)
                                {{$item->title}}
                            @endforeach
                        </p>
                        <p>
                            <strong>Ano</strong><br> {{ date('Y',strtotime($page->created_at)) }}
                        </p>
                    </div>
                    <div class="clear"></div>
                    <div class="content ">
                        {!! $page->content !!}

                    </div>
                </div>
            </div>
        </div>

@endsection
