<footer class="footer">
    <div class="footer-content center-relative">
        <div class="footer-mail">
            <a style="font-size:85%;" href="mailto:contato@agencialed.com.br" target="_blank">{{$information->email}}</a>
        </div>
        <div class="footer-number">
            <a href="https://wa.me/+55<?= preg_replace('/[^0-9]+/','', $information->whatsapp)?>" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i> {{$information->whatsapp}}</a>
            <br>
            <a href="tel:+{{$information->phone1}}"><i class="fa fa-phone" ></i> {{$information->phone1}}</a>
        </div>
        <div class="footer-info">
            {{$information->address}}, {{$information->number}}<br> {{$information->district}} <br> {{$information->city}}
        </div>

        <div class="social-holder">
            <a style="{{isset($information->linkedin)!=""?"":"display:none;"}}" href="{{$information->linkedin}}" target="_blank">
                <span class="fa fa-linkedin-square"></span>
            </a>
            <a style="{{isset($information->facebook)!=""?"":"display:none;"}}" href="{{$information->facebook}}" target="_blank">
                <span class="fa fa-facebook-square"></span>
            </a>
            <a style="{{isset($information->instagram)!=""?"":"display:none;"}}" href="{{$information->instagram}}" target="_blank">
                <span class="fa fa-instagram"></span>
            </a>
            <a style="{{isset($information->twitter)!=""?"":"display:none;"}}" href="{{$information->twitter}}" target="_blank">
                <span class="fa fa-twitter-square"></span>
            </a>
        </div>

        <div class="copyright-holder">
            <a style="color: #FF1E5C;">Agência LED © {{ now()->year }} </a> - Todos os direitos reservados.
        </div>
    </div>
</footer>
