 <header>
  <div class="site-wrapper">
        <div class="doc-loader">
            <img src="{{asset('img/preloader.gif')}}" alt="Cardea">
        </div>
        <div class="header-holder">
            <div class="menu-wrapper center-relative relative">
                <div class="header-logo">
                    <a href="{{ ($url!=route('nav.index')?route('nav.index'):route('nav.index').'#home') }}">
                        <img src="{{ asset('img/logo_led.png')}}" alt="Cardea">
                    </a>
                </div>

                <div class="toggle-holder">
                    <div id="toggle" class="">
                        <div class="first-menu-line"></div>
                        <div class="second-menu-line"></div>
                        <div class="third-menu-line"></div>
                    </div>
                </div>

                <div class="menu-holder">
                    <nav id="header-main-menu">
                        <ul class="main-menu sm sm-clean">
                            <li>
                                <a href="{{route('nav.index')}}#about">Somos</a>
                            </li>
                            <li>
                                <a href="{{route('nav.index')}}#services">Fazemos</a>
                            </li>
                            <li>
                                <a href="{{route('nav.index')}}#clients">Atendemos</a>
                            </li>
                            <li>
                                <a href="{{route('nav.index')}}#portfolio">Criamos</a>
                            </li>
                            <li>
                                <a href="{{route('nav.index')}}#news">Blog</a>
                            </li>
                            <li>
                                <a href="{{route('nav.index')}}#team">LEAG</a>
                            </li>
                            <li>
                                <a href="{{route('nav.index')}}#contact">Contato</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
 </header>
