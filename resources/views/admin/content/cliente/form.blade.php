@extends('layouts.admin')
@section('title', 'Páginas')
@section('content')
<form class="form-horizontal form-bordered" id="form_cadastre" method="post"
    action="{{ isset($entity->id)?route('content.edit.save',['id'=>$entity->id]):route('content.save') }}">
    @csrf
    <input type="hidden" name="type" value="{{ $_type }}">
    <header class="page-header">
        <h2>Clientes</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ route('information.index') }}">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li><span>Clientes</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
        </div>
    </header>
    <section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>

            <h2 class="card-title">Nome da Logo</h2>
            <p class="card-subtitle">
                Informações necessárias para a correta exibição.
            </p>
        </header>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-align-left"></i>
                                    </span>
                                </span>
                                <input value="{{ isset($entity->title)!=""?$entity->title:'' }}" type="text"
                                    name="title" class="form-control" placeholder="Nome da Logo" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="card">
        <header class="card-header">
            <div class="card-actions">
                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
            </div>
            <h2 class="card-title">Imagem da Publicação</h2>
            <p class="card-subtitle">
                Selecione a image para a inserção de banner.<br />
                <br />
                <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                    do campo.</strong>
            </p>
        </header>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-lg-12">
                    <small>Tamanho da imagem 250 x 85px</small>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-append">
                            <div class="uneditable-input">
                                <i class="fas fa-file fileupload-exists"></i>
                                <span class="fileupload-preview"></span>
                            </div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-exists">Trocar</span>
                                <span class="fileupload-new">Selecionar Imagem</span>
                                <input type="file" name='image' accept="image/*" onchange='loadPreview(this, 250,85)'
                                    {{ isset($entity)?'':'required' }}>
                            </span>
                            <a href="forms-basic.html#" class="btn btn-default fileupload-exists"
                                data-dismiss="fileupload">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <textarea id="base64" name="base64" style="display:none;"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="img-content">
                        <img id='output' class='img-fluid'
                            src='{{ isset($entity->image)!=""?"/content/".$entity->id."/".$entity->image:'' }}'>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="card">
        <div class="card-body" style="display: block;">
            <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                Salvar</button>
        </div>
    </section>
</form>
</div>
</div>



<script id="delete_gallery_template" type="x-tmpl-mustache">
    <input type="text" name="delete_gallery[]" class="form-control input-sm" placeholder="" value=" "/>
</script>
<script id="thumb_template" type="x-tmpl-mustache">
    <div class="gallery_item">
        <img src="@{{ thumb }}" class="img-responsive"/>
        <input type="hidden" name="gallery_image[]" value="@{{ original }}" />
        <input type="hidden" name="gallery_image_thumb[]" value="@{{ thumb }}" />
        <input type="text" name="gallery_image_descriptioreadAll[]" class="form-control input-sm" placeholder="Descrição da imagem"/>
        <a href="javascript:;" class="" id="gallery_item_remove">
            <i class="mdi mdi-delete"></i>Remover
        </a>
    </div>
</script>

@endsection
