@extends('layouts.admin')
@section('title', 'Portfólio')
@section('content')

<header class="page-header">
    <h2>Portfólio</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Portfólio</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ isset($entity->id)?route('content.edit.save',['id'=>$entity->id]):route('content.save') }}">
            @csrf
            <input type="hidden" name="type" value="{{ $_type }}">

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Informações básicas</h2>
                    <p class="card-subtitle">
                        Informações necessárias para a correta exibição.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->title)!=""?$entity->title:'' }}" type="text"
                                            name="title" class="form-control" placeholder="Titulo" required>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-align-left"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->year)!=""?$entity->year:'' }}"
                                            type="text" name="year" class="form-control" placeholder="Ano"
                                            required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <textarea class="form-control" name="description" rows="3" placeholder="Descrição">{{ isset($entity->description)!=""?$entity->description:'' }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle"
                            data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Imagem da Publicação</h2>
                    <p class="card-subtitle">
                        Selecione a image para a inserção de banner.<br />
                        <br />
                        <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição do campo.</strong>
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-flex justify-content-center">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">
                                                Escolha o tamanho da imagem
                                            </label>
                                            <select name="size" class="form-control" id="imageSize">
                                                <option value="none">Selecione ---</option>
                                                <option value="p_one" data-width="690" data-height="690">Grande (690px x 690px)</option>
                                                <option value="p_one_half" data-width="345" data-height="690">Média Vertical (345px x 690px)</option>
                                                <option value="p_one" data-width="690" data-height="345">Média Horizontal (690px x 345px)</option>
                                                <option value="p_one_half" data-width="345" data-height="345">Pequena (345px x 345px)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <small>Tamanho da imagem 690 x 345px</small>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fas fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Trocar</span>
                                        <span class="fileupload-new">Selecionar Imagem</span>
                                        <input id="image" type="file" name='image' accept="image/*"
                                            onchange='loadPreview(this)' {{ isset($entity)?'':'required' }}>
                                    </span>
                                    <a href="forms-basic.html#" class="btn btn-default fileupload-exists"
                                        data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <textarea id="base64" name="base64" style="display:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="img-content">
                                <img id='output' class='img-fluid'
                                    src='{{ isset($entity->image)!=""?"/content/".$entity->id."/".$entity->image:'' }}'>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Vídeo</h2>
                    <p class="card-subtitle">
                        Insira o link para exibir o vídeo. Caso deixe em branco a imagem selecionada acima será exibida.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-link"></i>
                                            </span>
                                        </span>
                                        <input value="{{ isset($entity->link)!=""?$entity->link:'' }}" type="text"
                                            name="link" class="form-control" placeholder="Link">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Conteudo</h2>
                    <p class="card-subtitle">
                        Insira o texto que será exibido ao clicar no item.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <textarea name="content"
                                        class="summernote">{{ isset($entity->content)?$entity->content:'' }}</textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Galeria de Imagens</h2>
                    <p class="card-subtitle">
                        Use o campo abaixo, e selecione as imagens para a galeria.
                    </p>
                </header>
                <div class="card-body">
                    <div class='form-group'>
                        <div class="col-md-12">
                            <input type="file" id="gallery-photo-add" class="inputfile gallery-photo-add" multiple accept="image/*"
                                data-id="">
                        </div>
                    </div>
                    <div class="progress progress-lg m-b-5">
                        <div id='progressBar' class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0"
                            aria-valuemin="0" aria-valuemax="100">

                        </div>
                    </div>
                    <div class="gallery">
                        @isset($gallery)
                        @foreach ( $gallery as $row )
                        <div class="gallery_item">
                            <img src={{ asset('content/'.$entity->id.'/thumb/'.$row->image) }} class="img-responsive" />
                            <input type="hidden" name="gallery_image_update_id[]" value="{{ $row->id }}" />
                            <input type="text" name="gallery_image_update_description[]" class="form-control input-sm"
                                placeholder="Descrição da imagem" value='{{ $row->description }}' />
                            <a href="javascript:;" class="" id="gallery_item_remove" data-id='{{ $row->id }}'>
                                <i class="mdi mdi-delete"></i>Remover
                            </a>
                        </div>
                        @endforeach
                        @endisset
                    </div>
                    <div id='gallery_item_remove_image'>
                    </div>
                </div>
            </section>

            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                        Salvar</button>
                </div>
            </section>

        </form>
    </div>
</div>



<script id="delete_gallery_template" type="x-tmpl-mustache">
    <input type="text" name="delete_gallery[]" class="form-control input-sm" placeholder="" value=" "/>
</script>
<script id="thumb_template" type="x-tmpl-mustache">
    <div class="gallery_item">
        <img src="@{{ thumb }}" class="img-responsive"/>
        <input type="hidden" name="gallery_image[]" value="@{{ original }}" />
        <input type="hidden" name="gallery_image_thumb[]" value="@{{ thumb }}" />
        <input type="text" name="gallery_image_descriptioreadAll[]" class="form-control input-sm" placeholder="Descrição da imagem"/>
        <a href="javascript:;" class="" id="gallery_item_remove">
            <i class="mdi mdi-delete"></i>Remover
        </a>
    </div>
</script>

@endsection
